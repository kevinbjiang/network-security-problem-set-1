#!/bin/bash
export CLASSPATH=lib/*:target/classes
java fcrypt -e receiver_public_key.pem sender_private_key.pem test ciphertext
java fcrypt -d receiver_private_key.pem sender_public_key.pem ciphertext plaintext

diff -q test plaintext
