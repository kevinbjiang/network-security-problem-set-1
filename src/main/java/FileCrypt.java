import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidParameterSpecException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kevinbjiang on 5/31/14.
 */
public class FileCrypt
{
    final static Logger logger = Logger.getLogger(FileCrypt.class.getName());

    final static String PUBLIC_KEY_CIPHER_ALGORITHM = "RSA";
    final static String PUBLIC_KEY_SIGNATURE_ALGORITHM = "SHA256withRSA";
    final static String PUBLIC_KEY_SIGNATURE_HASH_ALGORITHM = "SHA-256";

    private PEMKeyPairReader keyPairReader = new PEMKeyPairReader();
    private SymmetricCryptoHelper symmetricCryptoHelper = new SymmetricCryptoHelper();


    public void encryptAndSign(String recieverPublicKeyFile, String senderPrivateKeyFile, String inputFile, String outputFile)  throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidParameterSpecException, SignatureException
    {
        final PublicKey publicKey = keyPairReader.readPublicKey(recieverPublicKeyFile);
        final PrivateKey privateKey = keyPairReader.readPrivateKey(senderPrivateKeyFile);

        //generate aes key
        SecretKey secretKey = symmetricCryptoHelper.generateRandomKey();
        byte[] secretAesKey = secretKey.getEncoded();

        //encrypt aes key
        Cipher encryptCipher = Cipher.getInstance(PUBLIC_KEY_CIPHER_ALGORITHM);
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedAesKey = encryptCipher.doFinal(secretAesKey);

        //read in file
        Path inputFilePath = Paths.get(inputFile);
        byte[] inputFileBytes = Files.readAllBytes(inputFilePath);

        //encrypt file
        SymmetricCryptoEncryptResult encryptResult = symmetricCryptoHelper.encrypt(inputFileBytes, secretKey);

        //hash encrypted aes key, iv, encrypted bytes
        MessageDigest digest = MessageDigest.getInstance(PUBLIC_KEY_SIGNATURE_HASH_ALGORITHM);
        digest.update(encryptedAesKey);
        digest.update(encryptResult.getIv());
        digest.update(encryptResult.getCipherText());
        byte[] digestBytes = digest.digest();

        //sign digest result
        Signature signingSignature = Signature.getInstance(PUBLIC_KEY_SIGNATURE_ALGORITHM);
        signingSignature.initSign(privateKey);
        signingSignature.update(digestBytes);
        byte[] signatureBytes = signingSignature.sign();

        //write everything to file
        FileOutputStream fileOutputStream = null;
        try
        {
            fileOutputStream = new FileOutputStream(outputFile);
            fileOutputStream.write(signatureBytes);
            fileOutputStream.write(encryptedAesKey);
            fileOutputStream.write(encryptResult.getIv());
            fileOutputStream.write(encryptResult.getCipherText());
            fileOutputStream.flush();
        }
        catch (FileNotFoundException e)
        {
            logger.log(Level.SEVERE, "Unable to write to file: " + outputFile, e);
            throw e;
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE, "Unable to write to file: " + outputFile, e);
            throw e;
        }
        finally
        {
            if (fileOutputStream != null)
            {
                fileOutputStream.close();
            }
        }
    }

    public void verifyAndDecrypt(String senderPublicKeyFile, String receiverPrivateKeyFile, String inputFile, String outputFile) throws IOException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException
    {
        final PublicKey publicKey = keyPairReader.readPublicKey(senderPublicKeyFile);
        final PrivateKey privateKey = keyPairReader.readPrivateKey(receiverPrivateKeyFile);

        //open input file
        int offset = 0;
        Path inputFilePath = Paths.get(inputFile);
        byte[] inputFileBytes = Files.readAllBytes(inputFilePath);
        FileInputStream fileInputStream = new FileInputStream(inputFile);

        //copy signature
        byte[] signature = new byte[256];
        System.arraycopy(inputFileBytes, offset, signature, 0, 256);
        offset += 256;

        //get digest of content
        MessageDigest digest = MessageDigest.getInstance(PUBLIC_KEY_SIGNATURE_HASH_ALGORITHM);
        digest.update(inputFileBytes, 256, (inputFileBytes.length - 256));
        byte[] digestBytes = digest.digest();

        //validate signature
        Signature validatingSignature = Signature.getInstance(PUBLIC_KEY_SIGNATURE_ALGORITHM);
        validatingSignature.initVerify(publicKey);
        validatingSignature.update(digestBytes);
        boolean verified = validatingSignature.verify(signature);
        if (!verified)
        {
            throw new IllegalArgumentException("Signature does not match content in file: " + inputFile);
        }

        System.out.println("Signature matched file content");

        //read encrypted aes key
        byte[] encryptedAesKey = new byte[256];
        System.arraycopy(inputFileBytes, offset, encryptedAesKey, 0, 256);
        offset += 256;

        //read iv
        byte[] iv = new byte[16];
        System.arraycopy(inputFileBytes, offset, iv, 0, 16);
        offset += 16;

        //read encrypted data
        int encryptedDataSize = inputFileBytes.length - offset;
        byte[] encryptedData = new byte[encryptedDataSize];
        System.arraycopy(inputFileBytes, offset, encryptedData, 0, encryptedDataSize);

        //decrypt aes key
        Cipher decryptCipher = Cipher.getInstance(PUBLIC_KEY_CIPHER_ALGORITHM);
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedAesKey = decryptCipher.doFinal(encryptedAesKey);

        //decrypt data
        byte[] decryptedBytes = symmetricCryptoHelper.decrypt(encryptedData, iv, new SecretKeySpec(decryptedAesKey, PUBLIC_KEY_CIPHER_ALGORITHM));

        //write data to file
        FileOutputStream fileOutputStream = null;
        try
        {
            fileOutputStream = new FileOutputStream(outputFile);
            fileOutputStream.write(decryptedBytes);
            fileOutputStream.flush();

            System.out.println("File decrypted");
        }
        catch (FileNotFoundException e)
        {
            logger.log(Level.SEVERE, "Unable to write to file: " + outputFile, e);
            throw e;
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE, "Unable to write to file: " + outputFile, e);
            throw e;
        }
        finally
        {
            if (fileOutputStream != null)
            {
                fileOutputStream.close();
            }
        }
    }
}
