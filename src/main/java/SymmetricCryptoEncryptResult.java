
/**
 * Created by kevinbjiang on 5/31/14.
 */
public class SymmetricCryptoEncryptResult
{
    private byte[] iv;
    private byte[] cipherText;

    public byte[] getIv()
    {
        return iv;
    }

    public void setIv(byte[] iv)
    {
        this.iv = iv;
    }

    public byte[] getCipherText()
    {
        return cipherText;
    }

    public void setCipherText(byte[] cipherText)
    {
        this.cipherText = cipherText;
    }

    public SymmetricCryptoEncryptResult(byte[] iv, byte[] cipherText)
    {
        this.iv = iv;
        this.cipherText = cipherText;
    }
}
