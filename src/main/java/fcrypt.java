import javax.crypto.*;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidParameterSpecException;


public class fcrypt
{

    public static void main(String[] args)
    {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        if (args.length != 5)
        {
            printUsage();
            System.exit(-1);
        }

        FileCrypt fileCrypt = new FileCrypt();

        try
        {
            if (args[0].equalsIgnoreCase("-e"))
            {
                fileCrypt.encryptAndSign(args[1], args[2], args[3], args[4]);
            }
            else if (args[0].equalsIgnoreCase("-d"))
            {
                fileCrypt.verifyAndDecrypt(args[2], args[1], args[3], args[4]);
            }
            else
            {
                printUsage();
                System.exit(-1);
            }
        }
        catch (Exception e)
        {
            System.out.println("Fcrypt error: ");
            System.out.println(e.getMessage());
        }

    }

    public static void printUsage()
    {
        System.out.println("Usage:");
        System.out.println("");
        System.out.println("Encrypt file");
        System.out.println("java fcrypt -e <destination_public_key_filename> <sender_private_key_filename> <input_plaintext_file> <output_ciphertext_file>");
        System.out.println("");
        System.out.println("Decrypt file");
        System.out.println("java fcrypt -d <destination_private_key_filename> <sender_public_key_filename> <input_ciphertext_file> <output_plaintext_file>");
    }

}
