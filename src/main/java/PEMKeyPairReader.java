import org.bouncycastle.openssl.PEMReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kevinbjiang on 5/31/14.
 */
public class PEMKeyPairReader
{
    Logger logger = Logger.getLogger(PEMKeyPairReader.class.getName());

    public PublicKey readPublicKey(String path) throws IOException
    {
        //open public key file
        FileReader reader = null;
        try
        {
            reader = new FileReader(path);
        }
        catch (FileNotFoundException e)
        {
            logger.log(Level.SEVERE, "Unable to open file " + path, e);
            throw e;
        }

        //read public key
        PEMReader pemReader = new PEMReader(reader);
        Object pemObject = null;
        try
        {
            pemObject = pemReader.readObject();
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE, "Invalid pem format read " + path, e);
            throw e;
        }

        if (pemObject instanceof PublicKey)
        {
            return (PublicKey)pemObject;
        }
        else
        {
            logger.log(Level.SEVERE, "Unexpected pem object type " + pemObject.getClass().toString());
            throw new IllegalArgumentException(path + ": Unexpected pem object type " + pemObject.getClass().toString());
        }
    }

    public PrivateKey readPrivateKey(String path) throws IOException
    {
        //open private key file
        FileReader reader = null;
        try
        {
            reader = new FileReader(path);
        }
        catch (FileNotFoundException e)
        {
            logger.log(Level.SEVERE, "Unable to open file " + path, e);
            throw e;
        }
        PEMReader pemReader = new PEMReader(reader);

        //read private key
        Object pemObject = null;
        try
        {
            pemObject = pemReader.readObject();
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE, "Invalid pem format read " + path, e);
            throw e;
        }

        if (pemObject instanceof KeyPair)
        {
            KeyPair keyPair = (KeyPair)pemObject;
            return keyPair.getPrivate();
        }
        else
        {
            logger.log(Level.SEVERE, "Unexpected pem object type " + pemObject.getClass().toString());
            throw new IllegalArgumentException(path + ": Unexpected pem object type " + pemObject.getClass().toString());
        }
    }
}
