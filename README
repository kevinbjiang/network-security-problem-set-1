To build:

Run ./build.sh

Java 7 JDK is required and should be in the shell path

==========================================
To test:
Run ./runTests.sh

-The tests will:
    -Encrypt and sign the file called "test" and output a file called "ciphertext"
    -Validate signature and decrypt the file called "ciphertext" and output a file called "plaintext"
    -Diff "test" and "plaintext"

==========================================
To clean:

Run ./clean.sh

This will remove any build output or test output that was generated

==========================================
To run:

Before running, the classpath needs to be set:

export CLASSPATH=lib/*:target/classes

Then you can call fcrypt to encrypt and decrypt:

java fcrypt -e receiver_public_key.pem sender_private_key.pem test ciphertext
java fcrypt -d receiver_private_key.pem sender_public_key.pem ciphertext plaintext

===========================================
Overview

The program takes in private/public keys in PEM format, and either encrypt/signs or verifies/decrypts files. Because the encrypt/sign operation uses a custom file format for output, the verify/decrypt operation only works on files generated with this tool

There are tradeoffs between sign then encrypt vs encrypt then sign, but we choose to encrypt then sign. This provides a file integrity check without having to successfully decrypt, and the recipient can validate that the file belongs to a trusted entity before putting it through the decryption process.


Encryption

We want to avoid using public key encryption to encrypt the entire file for performance reasons. Therefore, the encrpytion operation first generates a 128-bit AES key, and encrypts the file using AES. The AES key is encrypted with the private key, which is then included in the resulting file. We choose AES for its battle-tested reputation. We could increase the key size to 192 or 256, but to do so in Java would require the JCE Unlimited Strength policies. However, the code is written in such a way that changing the key size is just a change of a constant. Finally we choose CBC as the block mode. No particular reason for this, other than that each block depends on the ciphertext of the previous block, which prevents many types of attacks. Really we just wanted to avoid the pitfalls of ECB mode.

Signing

Similar to encryption, we want to avoid performing a public key signing operation on a large file for performance reasons. So instead, we calculate a SHA-256 digest on the file and sign the result. The signature is then added to the output file, so that the receiver can validate the signature before decrypting. We choose SHA-256 because it is considered a safe cryptographic hash by the security community, unlike some others such as MD5.
